<img src="Heebo/resources/publish/Google Play/Banner/Heebo_banner.png" width="500" />

# Platforms
- Gnu/Linux
- Windows
- Mac OsX
- Android
- iOS

# Creators
<b>Luis Da Costa (LibreTrend)</b><br>
<i>New Code (2016 - Now)</i><br\>

<b>Nádia Gomes (LibreTrend)</b><br>
<i>New Design (2016 - Now)</i><br\>
<hr\>
<b>Mats Sjöberg</b><br>
<i>Original Code (2012)</i><br\>

<b>Niklas Gustafsson</b><br>
<i>Original Design (2012)</i><br\>

# Copyright and License

Copyright 2016 &copy; - Libretrend<br\>
Copyright 2012 &copy; - Mats Sjöberg, Niklas Gustafsson

All the source code and game level maps for Heebo are licensed under <b>GPLv3</b>.<br\>
All graphics are licensed under <b>CC-BY-SA</b>.