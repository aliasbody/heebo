TEMPLATE = app

QT += qml quick

CONFIG += c++11

HEADERS +=  resources/cpp/gameview.h      \
            resources/cpp/gamemapset.h    \
            resources/cpp/gamemap.h

SOURCES +=  resources/cpp/gameview.cpp    \
            resources/cpp/heebo.cpp       \
            resources/cpp/gamemapset.cpp  \
            resources/cpp/gamemap.cpp

RESOURCES +=    qml.qrc     \
                images.qrc  \
                js.qrc      \
                maps.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

ios {
    # Set "Minimum Target" for iOS
    QMAKE_IOS_DEPLOYMENT_TARGET = 8.0

    QMAKE_BUNDLE_DATA += assets_catalogs
    QMAKE_INFO_PLIST = $$PWD/platforms/ios/Info.plist

    assets_catalogs.files = $$files($$PWD/platforms/ios/Assets.xcassets)
}
