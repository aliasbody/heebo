TEMPLATE = app

QT += qml quick

CONFIG += c++11

HEADERS +=  resources/cpp/gameview.h      \
            resources/cpp/gamemapset.h    \
            resources/cpp/gamemap.h

SOURCES +=  resources/cpp/gameview.cpp    \
            resources/cpp/heebo.cpp       \
            resources/cpp/gamemapset.cpp  \
            resources/cpp/gamemap.cpp

RESOURCES +=    qml.qrc     \
                images.qrc  \
                js.qrc      \
                maps.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

android {
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/platforms/android

    DISTFILES +=    platforms/android/AndroidManifest.xml                       \
                    platforms/android/gradle/wrapper/gradle-wrapper.jar         \
                    platforms/android/gradlew                                   \
                    platforms/android/res/values/libs.xml                       \
                    platforms/android/build.gradle                              \
                    platforms/android/gradle/wrapper/gradle-wrapper.properties  \
                    platforms/android/gradlew.bat
}
