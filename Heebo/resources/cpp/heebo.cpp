/*
    Copyright 2016 - Libretrend <luisdc@libretrend.com>
    Copyright 2012 - Mats Sjöberg

    This file is part of the Heebo programme.

    Heebo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Heebo is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with Heebo.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QGuiApplication>
#include "gameview.h"

int main(int argc, char *argv[]) {
    QGuiApplication app(argc, argv);

    GameView view;
    view.setSource(QUrl(QStringLiteral("qrc:///resources/qml/main.qml")));
    view.setResizeMode(GameView::SizeRootObjectToView);

    // TODO: Find a better way to handle desktop (window) and mobile (fullscreen)
    #if (defined(Q_OS_WIN) || defined(Q_OS_MACX))
        view.show();
    #else
        view.showFullScreen();
    #endif

    return app.exec();
}
