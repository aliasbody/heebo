/*
    Copyright 2016 - Libretrend <luisdc@libretrend.com>
    Copyright 2012 - Mats Sjöberg
  
    This file is part of the Heebo programme.

    Heebo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Heebo is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with Heebo.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.4
import QtGraphicalEffects 1.0

import "qrc:///resources/js/jewels.js" as Jewels

JewelPage {
    id: mainPage
    
    property real buttonOffset: 0.0

    property bool isRunning: false

    property int dt: 0
    
    signal animDone()
    signal jewelKilled();
    
    SystemPalette { id: activePalette }

    Component.onCompleted: {
        Jewels.init();
        animDone.connect(Jewels.onChanges);
        jewelKilled.connect(Jewels.onChanges);
        okDialog.closed.connect(Jewels.dialogClosed);
        okDialog.opened.connect(tintRectangle.show);
    }

    function openFile(file) {
        var component = Qt.createComponent(file)

        if (component.status === Component.Ready) {
            loader.sourceComponent = component
        } else {
            console.log("Error loading component:", component.errorString());
        }
    }

    Loader{
        id: loader
        focus: true
        z: 100

        anchors.fill: parent

        anchors.bottomMargin: parent.height*0.15
        anchors.topMargin   : parent.height*0.05

        anchors.leftMargin  : parent.width*0.10
        anchors.rightMargin : parent.width*0.10

        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        function hide() {
            tintRectangle.hide()
            loader.sourceComponent = undefined
            loaderShadow.visible = false
        }

        onSourceComponentChanged: {
            tintRectangle.show()
            loaderShadow.visible = true;
        }

        DropShadow {
            id: loaderShadow;
            anchors.fill: loader
            cached: true;
            horizontalOffset: 3;
            verticalOffset: 3;
            radius: 8.0;
            samples: 16;
            color: "#80000000";
            visible: false
            source: loader;
        }
    }

    JewelDialog {
        id: okDialog
        anchors.centerIn: background
        z: 55
    }

    Item {
        id: background;
        width: parent.width
        anchors {
            top: parent.top;
            bottom: toolBar.top
        }

        MouseArea {
            anchors.fill: parent
            onPressed: Jewels.mousePressed(mouse.x, mouse.y)
            onPositionChanged: if (pressed) Jewels.mouseMoved(mouse.x, mouse.y)
        }
    }

    ToolBar {
        id: toolBar

        ToolBarExitButton {
            id: backButton
        }

        Row {
            anchors {
                left: parent.left
                verticalCenter: parent.verticalCenter
                leftMargin: Jewels.tool_bar_left_margin +
                            (currentLevelText.text.length == 1 ? Jewels.level_margin_1digit_offset : 0)
            }

            Text {
                text : Jewels.toolbar_level_text
                color: Jewels.color_uiaccent

                font.family     : Jewels.font_family
                font.pixelSize  : Jewels.fontsize_main
            }
            Text {
                id: currentLevelText
                text : "??"
                color: Jewels.color_main

                font.family     : Jewels.font_family
                font.pixelSize  : Jewels.fontsize_main
            }                
            Text {
                text : "/"
                color: Jewels.color_uiaccent

                font.family     : Jewels.font_family
                font.pixelSize  : Jewels.fontsize_main
            }                
            Text {
                id: lastLevelText
                text : "??"
                color: Jewels.color_main

                font.family     : Jewels.font_family
                font.pixelSize  : Jewels.fontsize_main
            }                
        }

        Image {
            id: menuButton

            source: "qrc:///resources/images/" + Jewels.imgFolder + "/icon_menu.png"
            width : toolBar.height * 0.800
            height: toolBar.height * 0.800

            smooth      : true
            antialiasing: true
            mipmap      : true

            anchors {
                right: parent.right
                verticalCenter: parent.verticalCenter
                verticalCenterOffset: -Jewels.menu_jump*mainPage.buttonOffset
                rightMargin: 20
            }

            MouseArea {
                anchors.fill: parent
                onClicked : mainMenu.toggle()
                onPressed : menuButton.source = "qrc:///resources/images/" + Jewels.imgFolder + "/icon_menu_pressed.png"
                onReleased: menuButton.source = "qrc:///resources/images/" + Jewels.imgFolder + "/icon_menu.png"
            }

            Behavior on anchors.verticalCenterOffset {
                SpringAnimation {
                    epsilon: 0.25
                    damping: 0.1
                    spring: 3
                }
            }

        }
    }

    Rectangle {
        id: tintRectangle
        anchors.fill: parent
        color: "#3399FF"
        opacity: 0.0
        visible: opacity > 0

        z: 10

        function show() {
            var colors = ["#3399FF", "#11FF00", "#7300E6", "#FF3C26",
                          "#B300B3" /*, "#FFD500"*/];

            tintRectangle.color = colors[Jewels.random(0,4)];
            tintRectangle.opacity = 0.65;
        }

        function hide() {
            tintRectangle.opacity = 0;
        }            

        MouseArea {
            anchors.fill: parent
            onClicked: {
                loader.hide();
                mainMenu.hide();
                if (!okDialog.isClosed())
                  okDialog.hide();
            }
        }

        Behavior on opacity {
            SmoothedAnimation { velocity: 2.0 }
        }
    }

   Image {
        id: mainMenu
        z: 50

        anchors {
            fill: parent

            bottomMargin: parent.height*0.12
            topMargin   : parent.height*0.12

            leftMargin  : parent.width*0.06
            rightMargin : parent.width*0.06

            verticalCenter: parent.verticalCenter
            horizontalCenter: parent.horizontalCenter

            verticalCenterOffset: Jewels.main_menu_offset
        }

        source: "qrc:///resources/images/" + Jewels.imgFolder + "/main_menu_bg.png"

        signal closed

        function toggle() {
            if (okDialog.isClosed()) {
              if (visible) { hide(); } else { show(); }
            }
        }
    
        function show() {
            loader.hide()
            mainMenu.opacity = 1
            tintRectangle.show()
            mainPage.buttonOffset = 1.0
        }
        
        function hide() {
            mainMenu.opacity = 0
            tintRectangle.hide()
            mainMenu.closed()
            mainPage.buttonOffset = 0.0
        }

        opacity: 0
        visible: opacity > 0

        Image {
            source: "qrc:///resources/images/" + Jewels.imgFolder + "/heebo_logo.png"
            width : parent.width * 0.700
            height: sourceSize.height / (sourceSize.width / width)

            smooth      : true
            antialiasing: true
            mipmap      : true

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.baseline        : parent.top
            anchors.baselineOffset  : -32
        }

        Grid {
            id: menuGrid
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter  : parent.verticalCenter
            spacing: 10
            columns: 2

            MenuButton {
                text: "New game"
                buttonImage         : "qrc:///resources/images/" + Jewels.imgFolder + "/icon_newgame.png"
                pressedButtonImage  : "qrc:///resources/images/" + Jewels.imgFolder + "/icon_newgame_pressed.png"
                onClicked: { mainMenu.hide(); Jewels.firstLevel() }
            }

            MenuButton {
                text: "Restart level"
                buttonImage         : "qrc:///resources/images/" + Jewels.imgFolder + "/icon_restart.png"
                pressedButtonImage  : "qrc:///resources/images/" + Jewels.imgFolder + "/icon_restart_pressed.png"
                onClicked: { mainMenu.hide(); Jewels.startNewGame() }
            }

            MenuButton {
                text: "Help"
                buttonImage         : "qrc:///resources/images/" + Jewels.imgFolder + "/icon_help.png"
                pressedButtonImage  : "qrc:///resources/images/" + Jewels.imgFolder + "/icon_help_pressed.png"
                onClicked: { mainMenu.hide(); openFile("HelpPage.qml") }
            }

            MenuButton {
                text: "About"
                buttonImage         : "qrc:///resources/images/" + Jewels.imgFolder + "/icon_about.png"
                pressedButtonImage  : "qrc:///resources/images/" + Jewels.imgFolder + "/icon_about_pressed.png"
                onClicked: { mainMenu.hide(); openFile("AboutPage.qml") }
            }
        }
    }
}

