/*
    Copyright 2016 - Libretrend <luisdc@libretrend.com>
    Copyright 2012 - Mats Sjöberg
  
    This file is part of the Heebo programme.

    Heebo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Heebo is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with Heebo.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.4

import "qrc:///resources/js/constants.js" as Constants

Rectangle {
    id: mainPage

    width : Constants.block_width*Constants.board_width
    height: Constants.block_height*Constants.board_height+Constants.toolbar_height

    // Temporary Splash Location
    Rectangle {
        id: mainRect
        z :1
        anchors.fill: parent

        gradient: Gradient {
            GradientStop {
                position: 0.0
                color: "#87E0FD"
            }
            GradientStop {
                position: 0.4
                color: "#53CBF1"
            }
            GradientStop {
                position: 1.0
                color: "#05ABE0"
            }
        }

        Image {
            id: splashLogo

            source: "qrc:///resources/images/" + Constants.imgFolder + "/heebo_logo.png"
            width : parent.width * 0.700
            height: sourceSize.height / (sourceSize.width / width)

            smooth      : true
            antialiasing: true
            mipmap      : true

            anchors.centerIn: parent
            anchors.verticalCenterOffset: -parent.height * 0.070
        }

        Text {
            id: splashByText

            text : "by"
            color: "#FFFFFF"

            //style       : Text.Outline
            //styleColor  : "#A0A0A0"

            font.bold       : true
            font.italic     : true
            font.pixelSize  : Constants.fontsize_endDialog

            anchors.top: splashLogo.bottom
            anchors.left: splashLogo.left
            anchors.leftMargin: parent.width * 0.015
        }

        Text {
            id: splashLibretrendText

            text : "Libretrend"
            color: "#FFFFFF"

            //style       : Text.Outline
            //styleColor  : "#A0A0A0"

            font.bold       : true
            font.italic     : true
            font.pixelSize  : Constants.fontsize_dialog

            anchors.top: splashLogo.bottom
            anchors.left: splashByText.right
            anchors.leftMargin: parent.width * 0.020
        }

        NumberAnimation on opacity {
            id: fadeOut
            running: false
            from: 2.0
            to  : 0.0
            duration: 2000
        }
    }

    // This is the Loader that will load the main.qml file
    Loader {
        id: loader
        z : 0
        anchors.fill: parent
        asynchronous: true
        onSourceChanged: fadeOut.start();
    }

    Timer {
        interval: 2000
        running : true
        repeat  : false

        onTriggered: { loader.source = "qrc:///resources/qml/MainPage.qml"}
    }
}
