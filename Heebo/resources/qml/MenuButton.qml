/*
    Copyright 2016 - Libretrend <luisdc@libretrend.com>
    Copyright 2012 - Mats Sjöberg

    This file is part of the Heebo programme.

    Heebo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Heebo is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with Heebo.  If not, see <http://www.gnu.org/licenses/>.
    */

import QtQuick 2.4

import "qrc:///resources/js/constants.js" as Constants

Item {
    id: container
    
    property string text: "OK";
    property string buttonImage: "";
    property string pressedButtonImage: "";

    signal clicked
    /* signal pressAndHold */

    width   : (Constants.block_width*Constants.board_width) / 3
    height  : (Constants.block_width*Constants.board_width) / 3


    Image {
        id: imageComponent

        source: container.buttonImage
        width : parent.width / 1.50
        height: parent.width / 1.50

        smooth      : true
        antialiasing: true
        mipmap      : true

        anchors {
            top: container.top
            horizontalCenter: container.horizontalCenter
        }
    }

    Text {
        id: textComponent

        text    : container.text
        color   : Constants.color_dark

        font.family     : Constants.font_family
        font.pixelSize  : Constants.fontsize_dialog * 0.50

        anchors {
            top: imageComponent.bottom
            horizontalCenter: container.horizontalCenter
        }
    }

    MouseArea {
        id: mouseArea

        anchors.fill: container

        onClicked : container.clicked();
        onPressed : imageComponent.source = container.pressedButtonImage
        onReleased: imageComponent.source = container.buttonImage
        /* onPressAndHold: container.pressAndHold(); */
    }
}
