/*
    Copyright 2016 - Libretrend <luisdc@libretrend.com>
    Copyright 2012 - Mats Sjöberg

    This file is part of the Heebo programme.

    Heebo is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Heebo is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
    License for more details.

    You should have received a copy of the GNU General Public License
    along with Heebo.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.4

import "qrc:///resources/js/constants.js" as Constants

Text {
    property string style: "normal"
    
    font.pixelSize: style === "header"   ?  parent.width * 0.14 :
                    style === "title"    ?  parent.width * 0.04 :
                    style === "small"    ?  parent.width * 0.03 :
                    style === "emphasis" ?  parent.width * 0.04 :
                                            parent.width * 0.04
    font.bold   : style === "title" || style === "header" || style == "emphasis"
    font.family : Constants.font_family
    color       : Constants.color_info

    textFormat  : Text.RichText
    wrapMode    : Text.Wrap

    onLinkActivated: Qt.openUrlExternally(link)
    
    anchors {
        left        : parent.left
        topMargin   : 22
        leftMargin  : 20
    }

    width: parent.width-35
}
