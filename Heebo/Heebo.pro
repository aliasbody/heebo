TEMPLATE = app
TARGET = Heebo

load(ubuntu-click)

QT += qml quick

HEADERS +=  resources/cpp/gameview.h      \
            resources/cpp/gamemapset.h    \
            resources/cpp/gamemap.h

SOURCES +=  resources/cpp/gameview.cpp    \
            resources/cpp/heebo.cpp       \
            resources/cpp/gamemapset.cpp  \
            resources/cpp/gamemap.cpp

RESOURCES +=    qml.qrc     \
                images.qrc  \
                js.qrc      \
                maps.qrc

CONF_FILES +=  platforms/ubuntu/Heebo.apparmor \
               platforms/ubuntu/Heebo.png

#show all the files in QtCreator
OTHER_FILES += $${CONF_FILES} \
               $${QML_FILES} \
               Heebo.desktop

#specify where the config files are installed to
config_files.path = /Heebo/platforms/ubuntu
config_files.files += $${CONF_FILES}
INSTALLS+=config_files

#install the desktop file, a translated version is 
#automatically created in the build directory
desktop_file.path = /Heebo/platforms/ubuntu
desktop_file.files = $$OUT_PWD/platforms/ubuntu/Heebo.desktop
desktop_file.CONFIG += no_check_exist
INSTALLS+=desktop_file

# Default rules for deployment.
target.path = $${UBUNTU_CLICK_BINARY_PATH}
INSTALLS+=target

